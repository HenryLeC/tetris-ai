from game import Game
import sys
from geneticSearch import Genetic_Search, train


def main():
    # textfile = open("./src/data/expectiminimax/10.csv", "w")
    # textfile.write("dropped, rows\n")
    # for i in range(2):
    #     print(i)
    #     g = Game(sys.argv[1])
    #     dropped, rows = g.run_no_visual()
    #     textfile.write(str(dropped) + ", " + str(rows) + "\n")
    # textfile.close()
    g = Game(
        "genetic",
        Genetic_Search(
            [
                0.41777563713829524,
                0.6806211357948476,
                0.22003146274653412,
                -0.25136291674893085,
            ]
        ),
    )
    g.run_no_visual()
    # g.run()
    # print(
    #     train(
    #         50,
    #         200,
    #         # [
    #         #     [
    #         #         0.9028226330041054,
    #         #         0.6085491375434718,
    #         #         0.23591877682882106,
    #         #         -0.01626130672894671,
    #         #     ],
    #         #     [
    #         #         1.1380846807435945,
    #         #         1.0381797426416384,
    #         #         0.5233434658106099,
    #         #         0.04876235014687219,
    #         #     ],
    #         # ],
    #         # [4745, 2720],
    #     )
    # )


if __name__ == "__main__":
    main()
