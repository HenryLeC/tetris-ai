from copy import deepcopy
from piece import Piece
from board import Board
from random import random
from typing import List
from game import Game
import json

"""
Performs a heuristic search of depth = 1
Generates all possible placements with the current piece
(all possible horizontal positions, all possible rotations)
chooses the placement that minimizes the cost function
"""

A = 0.5
B = 0.5


class Genetic_Search:
    def __init__(self, params=None):
        if params is None:
            self.params = []
            for i in range(4):
                self.params.append(random() * 2 - 1)
        else:
            self.params = params

    def get_best_move(self, board: Board, piece: Piece, depth: int = 1):
        best_x = -1
        best_piece = None
        min_cost = 100000000
        for i in range(4):
            piece = piece.get_next_rotation()
            for x in range(board.width):
                try:
                    y = board.drop_height(piece, x)
                except Exception:
                    continue
                c = self.cost(board, x, y, piece)
                if c < min_cost:
                    min_cost = c
                    best_x = x
                    best_piece = piece
        return best_x, best_piece

    def cost(self, board: Board, x: int, y: int, piece: Piece):
        board_copy = deepcopy(board.board)

        for pos in piece.body:
            board_copy[y + pos[1]][x + pos[0]] = True

        num_cleared = 0
        holes = 0
        max_height = 0
        agg_height = 0
        # Check for cleared rows and holes
        for i, row in enumerate(board_copy):
            if all(row):
                num_cleared += 1
            for j, block in enumerate(row):
                if block:
                    max_height = max(max_height, i)
                    # filled, can't be a hole
                    continue
                has_overhang = False
                for overhang_row in board_copy[i + 1 :]:
                    if overhang_row[j]:
                        has_overhang = True
                        break
                if has_overhang:
                    # has a block above
                    holes += 1

        heights = []
        bumpiness = 0
        for col in range(len(board_copy[0])):
            height = 0
            for row in range(len(board_copy)):
                if board_copy[row][col]:
                    height = row
            agg_height += height
            heights.append(height)

        for i in range(len(heights) - 1):
            bumpiness += abs(heights[i] - heights[i + 1])

        c = (
            self.params[0] * agg_height
            + self.params[1] * holes
            + self.params[2] * bumpiness
            + self.params[3] * num_cleared
        )
        return c


def train(
    num_agents: int,
    iterations: int,
    params: List[List[int]] = None,
    scores_start: List[int] = None,
    iter: int = None,
):
    agents: List[Genetic_Search] = []
    scores = []

    if params is None:
        for i in range(num_agents):
            agents.append(Genetic_Search())
    else:
        mixed = breed(params[0], scores_start[0], params[-1], scores_start[-1])
        for i in range(num_agents):
            agents.append(Genetic_Search(mutate(mixed, iter or 1, 0.5)))

    for iter in range((iter or 1), iterations + (iter or 1) + 1):
        for i in range(num_agents):
            g = Game("genetic", agents[i])
            scores.append(g.run_no_visual()[0])

        # Get the top two scores in the list
        top_scores = sorted(scores, reverse=True)[:2]
        print("Top Scores: ", top_scores)
        top_agents = []
        for i, score in enumerate(scores):
            if score in top_scores:
                top_agents.append(agents[i])

        # Create a new generation from the top two agents
        param1 = top_agents[0].params
        param2 = top_agents[1].params
        if iter == iterations - 1:
            return param1

        with open(f"scores/{iter}.json", "w") as f:
            json.dump({"scores": [top_scores[0], top_scores[1]], "weights": [param1, param2], "iter": iter}, f)

        print([top_scores[0], top_scores[1]])
        print([param1, param2])

        mixed = breed(param1, top_scores[0], param2, top_scores[1])

        new_agents = []
        for i in range(num_agents - 2):
            params = mutate(mixed, iter, 0.5)
            new_agents.append(Genetic_Search(params))
        new_agents.append(Genetic_Search(param1))
        new_agents.append(Genetic_Search(param2))

        agents = new_agents
        scores = []


def breed(genes1, score1, genes2, score2):
    mixed = []

    for i in range(len(genes1)):
        mixed.append((genes1[i] * score1 + genes2[i] * score2) / (score1 + score2))

    return mixed


def mutate(genes, iteration, gain):
    return [i + (random() * gain / iteration) - gain / (2 * iteration) for i in genes]
